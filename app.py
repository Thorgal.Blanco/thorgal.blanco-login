from http import HTTPStatus
from flask import Flask, abort, request, send_from_directory, make_response, render_template
from werkzeug.datastructures import WWWAuthenticate
import flask
from login_form import LoginForm
from json import dumps, loads
from base64 import b64decode
import sys
import apsw
from apsw import Error
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token;
from threading import local
from markupsafe import escape
import hashlib
import secrets

tls = local()
inject = "'; insert into messages (sender,recpient,message) values ('foo', 'foo foo', 'bar');select '"
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
conn = None

# Set up app
app = Flask(__name__)
# The secret key enables storing encrypted session data in a cookie (make a secure random key for this!)
app.secret_key = hashlib.sha256(str(secrets.randbelow(10**15)).encode('utf-8')).hexdigest() #gets a random number below 10^15 (just a high umber so it is hardly possible to brute-force)

# Add a login manager to the app
import flask_login
from flask_login import login_required, login_user
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"
    

# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass


# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    print(user_id)
    if not userExist(user_id):
        return

    # For a real app, we would load the User from a database or something
    user = User()
    user.id = user_id
    return user


# This method is called to get a User object based on a request,
# for example, if using an api key or authentication token rather
# than getting the user name the standard way (from the session cookie)
@login_manager.request_loader
def request_loader(request):
    # Even though this HTTP header is primarily used for *authentication*
    # rather than *authorization*, it's still called "Authorization".
    auth = request.headers.get('Authorization')

    # If there is not Authorization header, do nothing, and the login
    # manager will deal with it (i.e., by redirecting to a login page)
    if not auth:
        return

    (auth_scheme, auth_params) = auth.split(maxsplit=1)
    auth_scheme = auth_scheme.casefold()
    if auth_scheme == 'basic':  # Basic auth has username:password in base64
        (uid,passwd) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
        u_password = getPassword(uid)
        if check_password(getPassword(u_password), passwd):
            return user_loader(uid)


    #elif auth_scheme == 'bearer': # Bearer auth contains an access token;
        # an 'access token' is a unique string that both identifies
        # and authenticates a user, so no username is provided (unless
        # you encode it in the token – see JWT (JSON Web Token), which
        # encodes credentials and (possibly) authorization info)
        #TODO change users to refer to database and not dictionary for tokens

        #for uid in users:
        #    if users[uid].get('token') == auth_params:
        #        return user_loader(uid)
    
    
    
    # For other authentication schemes, see
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

    # If we failed to find a valid Authorized header or valid credentials, fail
    # with "401 Unauthorized" and a list of valid authentication schemes
    # (The presence of the Authorized header probably means we're talking to
    # a program and not a user in a browser, so we should send a proper
    # error message rather than redirect to the login page.)
    # (If an authenticated user doesn't have authorization to view a page,
    # Flask will send a "403 Forbidden" response, so think of
    # "Unauthorized" as "Unauthenticated" and "Forbidden" as "Unauthorized")

    #abort(HTTPStatus.UNAUTHORIZED, www_authenticate = WWWAuthenticate('Basic realm=inf226, Bearer'))

def pygmentize(text):
    if not hasattr(tls, 'formatter'):
        tls.formatter = HtmlFormatter(nowrap = True)
    if not hasattr(tls, 'lexer'):
        tls.lexer = SqlLexer()
        tls.lexer.add_filter(NameHighlightFilter(names=['GLOB'], tokentype=token.Keyword))
        tls.lexer.add_filter(NameHighlightFilter(names=['text'], tokentype=token.Name))
        tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
    return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'

#TODO: extract this to other file
@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'favicon.png', mimetype='image/png')


@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return send_from_directory(app.root_path,
                        'index.html', mimetype='text/html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        u_password = getPassword(username)
        if check_password(u_password, username + password): # checks if the given password is correct
            user = user_loader(username)
            
            # automatically sets logged in session cookie
            login_user(user)

            flask.flash('Logged in successfully.')

            next = flask.request.args.get('next')
    
            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index'))
    return render_template('./login.html', form=form)

@app.get('/search')
def search():
    query = request.args.get('q') or request.form.get('q') or '*'
    stmt = "SELECT * FROM messages WHERE message GLOB '%(query)s' AND (sender='%(id)s' OR recipient='%(id)s')" % {"query" : query, "id": flask_login.current_user.id}
    result = f"Query: {pygmentize(stmt)}\n"
    try:
        c = conn.execute(stmt)
        rows = c.fetchall()
        result = result + 'Result:\n'
        for row in rows:
            result = f'{result}    {dumps(row)}\n'
        c.close()
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

@app.route('/send', methods=['POST','GET'])
def send():
    try:
        sender = request.args.get('sender') or request.form.get('sender')
        message = request.args.get('message') or request.args.get('message')
        recipient = request.args.get('recipient') or request.args.get('recipient')
        recipients = recipient.split(', ')
        if not sender or not message:
            return f'ERROR: missing sender or message'
        if flask_login.current_user.id != sender:
            return f'ERROR: no access to send from user {sender}'
        for elem in recipients:
            if not userExist(elem):
                return f'ERROR: I am sorry {sender}, but there is no one named {elem} here , have you tried messaging bob instead?'
        #makes sure all the recipients are registered users before proceding to message them.
        for elem in recipients:
            stmt = "INSERT INTO messages (sender, recipient, message) values ('%s' , '%s' , '%s');" % (sender, elem, message)
            result = f"Query: {pygmentize(stmt)}\n"
            conn.execute(stmt)
        return f'{result}ok'
    except Error as e:
        return f'{result}ERROR: {e}'

@app.get('/announcements')
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}

@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp


try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        recipient TEXT NOT NULL,
        message TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS user (
        "password"	TEXT NOT NULL,
        "user_name"	TEXT NOT NULL UNIQUE);''')
        
except Error as e:
    print(e)
    sys.exit(1)



#####################
##method to compare users password to the given password

def check_password(actual_password, given_password):
    hashed = hashlib.sha256(given_password.encode('utf-8')).hexdigest() #hashing it since it is hashed in database
    return hashed == actual_password


#checks if the string has any special characters (non-number or letter)
def hasSpecialChar(text):
    for char in text:
        if not char.isalnum():
            return True
    return False

#adding a user to the database
def addUser(userName, password):
    try:
        ##need to restrict user from passing special characters when adding user so that we can avoid sql-injection
        if(not hasSpecialChar(userName) and not hasSpecialChar(password)):
            hashed = hashlib.sha256((userName + password).encode('utf-8')).hexdigest()
            c.execute(f'INSERT INTO user VALUES ("{hashed}", "{userName}")')#not a risk to sql-injection as there are no special characters in either hashed or username
        else:
            raise TypeError("Only integers and letters are allowed")
    except TypeError as e:
        print(e)
    except Error as e:
        print(e)

#adding the users to the database with hashed passwords (commented out as i have already added them)
#addUser('alice', 'password123')
#addUser('bob', 'bananas')

#checks if a user exists
def userExist(user_id):
    c.execute('SELECT user_name from user')
    for name in c: 
        if(user_id == name[0]):
            return True
    return False

#gets the hashed password of a user
def getPassword(user_id):
    c.execute(f'SELECT password, user_name from user')
    #itterating through comparing user_id with the user_names in the database, returning password if they match
    for elem in c:
        password = elem[0]
        user_name = elem[1]
        if (user_name == user_id):
            return password


##############################