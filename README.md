
# Security issues and how I resolved them


## Most notable, and inscure:
    Password:
        Stored as string, could get leaked. Should instead save in databased and hash it.
        Right now, doesnt need password to log in, should make correct password essential for login
    SQL-injection:
        Possible for sql-injection by typing sql-code in message. for example sending:   '); DROP table messages;--
            This is because the stmt variable which the message is sent to, is essentially sending an sql-command (INSERT INTO)
            , and '); ends this command and DROP table messages is the second command being sent. also -- comments out the ');
    Structure:
        Everything is packed inside one python code, could refactor, such as bring password and username to database instead.
        Putting password and username in database would also make it easier to add new users.


## refactoring:
    There is some methods without any real functionality, such as the "nocoffee" and "gotcoffee" methods, which is not called
    upon. I REMOVE THE COFFEE METHODS!!!!!!        
            @app.get('/coffee/')
            def nocoffee():
                abort(418)

            @app.route('/coffee/', methods=['POST','PUT'])
            def gotcoffee():
                return "Thanks!"'

    removing print's as these are unecissary for the functionality of the program.




### randomizing el secret_key:
    I randomize the secret_key to make it harder to guess (bruteforce). Now the secret_key is practically imposible to guess.
    This makes it hard for any potential attacker to get the key, which makes it so that no attacker gets access.

### Password to database:
    I made a new column in tiny.db called user. This stores the user_name and password both as strings.

### checking password:
    i changed the variable u in login to u_password as we only need to get the password of the given username. to get the password,
    i started off with: cursor.execute(f'SELECT password, user_name from user WHERE user_name = "{user_id}"'), but realized
    that this was subceptible to sql-injections by typing into password, so i changed it to instead itterate through all names
    in the database and return the password of the matching username. we call on this function in the login function to get
    the password of the user in the database, so that we can compare with the users given password in the input-field.

### hashing password:
    I used pythons hashlib to hash the password with sha256. By doing this, i can still compare the passwords, but even if any
    passwords gets leaked, they only get to see the hashed password.
    another security measure:
        by simply hashing the passwords, if the passwords gets leaked, it will be able to see users that has the same passwords,
        because same paswords produce same hash. To fix this, we need to add "salt" to the encrypting. I took a quick-fix and 
        simply added the user_name string to the start of the password when encrypting. This makes it so that identical
        passwords will have a different hashed value.

### protecting against SQL-injection:
    It is possible with sql-injection by writing in the from or message then sending. To fix this, we simply have to fix the way we
    handle the parameters 'sender' and 'message' in the send method. The query in the search method, is not subceptible to sql-injection
    as it will throw an "SQLError: near syntax error" if attempted. 
    Stopping sql-injections have improved confidentiality, integrity and availability, by removing the possibility of injecting:
    f.ex. DROP table messages (availability), INSERT INTO messages (integrity), SELECT * FROM messages (confidentiality).



## Adding functionality:

### sender and resciever:
    I made it so that a user can only send from themselfes (they must write their own username in From to send it). I did this by simply 
    setting a requirement in the send method:
        if flask_login.current_user.id != sender:
            return f'ERROR: no access to send from user {sender}'
    Essentially, when you try to send from someone that is not you, it returns an error.
    I made it so that there can be one or more recipients of the message. I extended the message table in the database to have a column
    'recipient' that keep track of who recieved the message. I made another user-input bar called To: where you can insert the recipients.
    Sending to several recipients can be done by separating them with ', '. In order to send the message, all recipients must be a user.
    In the case of several recipients, there is sent several messages, one for each recipient.

    Implementing this increases integrity by making it so that noone can pose as another user when sending the message, since they have to 
    write their own user_name. It also improves confidentiality if we make it so that you can only read a message if you are either the 
    recipient or sender (not implemented yet).

### limited read access of user:
    I am making it so that a user can only show messages that he/herself has sent or is recipient of. i will do this by filtering with 
    WHERE user_name == user.id OR recipient == user.id:
        stmt = "SELECT * FROM messages WHERE message GLOB '%(query)s' AND (sender='%(id)s' OR recipient='%(id)s')" % {"query" : query, "id": flask_login.current_user.id}
    Now, when a user searches for the messages, only their sent or recieved messages will be displayed. By doing this, we have improved
    the confidentiality by stopping outsiders from reading your love-letters.


## Our result so far (in short):
   We have made many security improvements. we have: 
   1. made the passwords more sequre by hashing them with sha256
   2. randomized the secret_key to stop outsiders from accessing/guessing it
   3. requireing password to lig in, icreasing security by denying access to people not knowing the password.
   4. removed the possibility of sql-injection by imporving the design: simply changing the way we use the parameter variables in c.execute

   by added functionality:
   5. a user can only send messages from its own user, not using another identity.
   6. a user can only read his/her own sent/recieved messages


# Testing/demoing the application
   ## Keep in mind while going through:
      there are 2 existing users in the database. alice and bob. alice's password is 'password123' and bob's password is 'bananas'
      The username and password are case sensitive


   ## Starting at login-page:
   ### for each test, start of by doing:
   1. start the server by using the flask run command.
   2. open localhost:5000 in the web browser.
   
   ### Testing for password authentication:
   1. You should now be in the login page, now type in bob in username field, and 'banana' in the password field
   Notice! 
      you can now se that nothing happened, and we are still in the login page. This is due to 'banana' being the wrong password
   2. Now, keep bob in username input, and type 'bananas' in the password input. 
   Notice!
      You should now have advanced to the "messaging page". this is due to the currect username and password (authentication).
   3. If wanted, you can try the same steps again, starting at login-page, but with user 'alice' with password 'password123'

   ## Starting in messaging-page:
   1. get to the messaging-page by following "Testing for password authentication" step 1 and 2
   Notice!
      you are now logged in as 'bob', currently the most active user in the database.
   ### Send message:
   2. you should now see 4 input boxed. Search, From, To and Message. we ignore Search for now. Now type in the following:
      In from-input: 'alice', In To-input: 'alice, bob, bob, pål', In Message-tab: 'du er en Nalle'.
      Now press the send-button.
   Notice!
      at the top of the screen, you should be able to see an error message saying "no access to send from user alice".
      This is due to bob only being alowed to send messages in his own name.
   3. now, change the From-input from 'alice' to 'bob' and press the send-button again.
   Notice!
      you should now have recieved the error message:  'I am sorry bob, but there is no one named pål here , 
      have you tried messaging bob instead?'
      This is due to pål not being an registered user. 
      We did not get the "no access to send from user alice" this time, since we wrote From bob and are logged in as bob
   4. now, change the To-input to: 'alice, bob, bob' (remove pål)
   notice!
      You should not have recieved any error messages this time, as we sent the message to existing users.
   ### show messages
   5. now, press the "show all"-button.
   Notice!
      3 messages should be displayed, 2 with bob as sender and reciver and 1 with bob as sender and alice as reciever
   6. now, go back to http://localhost:5000/login?next=%2F, and log in as alice (follow "starting at login-page" step 3)
   7. now, press the "show all"- button again.
   Notice!
      only 1 message should be displayed, message 3. This message should be sent by bob and rescieved by alice.
      This is because alice only has access to messages that she either sends or recieves. 
   ### attempt sql-injection:
   8. now, change From-input to alice, To input to alice and Message-input to "') DROP table messages; --", then click send
   Notice!
      You should see an Error displayed: "ERROR: SQLError: near "DROP": syntax error", and the sql-injectio should therefore have failed
   9. now, press the "show all"-button again
   Notice!
      The messages are still there, and therefore te messages table still exists.


# Answer to questions:
   Before attempting to make the program secure:
   ### who might attack?
      Attackers of the application could be 1. people who simply want to casue caos, 2. people wanting to know the contents of an
      confidential message (maybe a jealous boyfriend?), 3. Other messaging platforms wanting to ruin for their competition.
   ### possible attacks (what can an attacker do?):
      The attacker can make many types of sql-injections. by typing certain commands into from, to or message input, the user
      can drop tables from the database, access the user credentials (if the user credentials are stored in the database and not the
      python file.) and other malicious actions.
      The attacker could get the messages from the client and server by using the fixed key: "s3cret_key" that were used to see what is
      sent from and to the server. also, cookies will be stored and one can login without having to type in user credentials.
      The attacker can see the password being sent to the server if the secret_key is known, and therefore access the user's account.
      This is because passwords are not hashed.
      Initially, passwords are not even required, makin it even easier for an attacker to access another users account
      

   ### What damage can be done:
      Integrity: 
         Integrity can be degraded by accessing another users account (through getting their password) and sendign a message 
      to a user identified as someone else.
         Sql-injection by altering messages through sql-commands
         A user can send in another users name, when being logged in to another users account.

      Confidentiality:
         Sql-injection can degrade confidentiality by accessing messegas calling commands such as SELECT x FROM y.
         everyone can see all messages by clicking on "show all"-button, and therefore messages can not be sent to
      another person with confidentiality.
         A person with the secret_key can read the messages between client and server of another user. This puts
      confidentiality at risk

      availability:
         sql-injections such as drop table can make it so that users no longer have access to the messages, because
      they were all deleter. It could also drop a user table if user credentials are stored in the database.
         probably subceptible to ddos attacks if run on a propper server.

   ## What we should do/ what i have done:
   ### explained more at the start of README.md (everything down to testing/demo)

   ## access control model
   The only functional access model i have implemented is through username and password, where all who log in have the same
   access level.

   ## How to know that the security is good eanough:
   You should log what's happening in the application. Such as failed authentication attempts, cookies used to access,
   runtime errors and connection problems.
   If these logs occur often, it is probably worth taking a closer look into them.
   





